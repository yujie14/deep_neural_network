from PIL import Image
import math
import numpy as np
import os
import tensorflow as tf
import matplotlib.pyplot as plt
from PIL import Image,ImageDraw
image_path = 'img_172.jpg'
img = Image.open(image_path)
print()
img = img.resize((120, 160))
img_weight, img_height = img.size
inputs = []
img = np.array(img.getdata())
input = np.reshape(img, (img_weight, img_height, 3))
inputs.append(input)
inputs = np.array(inputs)

n_input = [120, 160, 3]
n_output = 4

x = tf.placeholder(tf.float32, [None, n_input[0], n_input[1], n_input[2]])
y = tf.placeholder(tf.float32, [None, n_output])
keep_prob = tf.placeholder(tf.float32)
lr = tf.placeholder(tf.float32)
model_path = "model.ckpt"

weights = {}
bias = {}


def norm(name, l_input, lsize=4):
    return tf.nn.lrn(l_input, lsize, bias=1.0, alpha=0.001 / 9.0, beta=0.75, name=name)


def _weight_variable(name, shape):
    return tf.get_variable("w" + str(name), shape, initializer=tf.orthogonal_initializer())


def _bias_variable(name, shape):
    return tf.get_variable("b" + str(name), shape[3])


def conv_block(input, name, shape, strides=[1, 1, 1, 1], padding="SAME", add_relu=True):
    weight = _weight_variable(name, shape)
    bias = _bias_variable(name, shape)
    conv = tf.nn.conv2d(input, weight, strides, padding=padding)
    pre_activation = tf.nn.bias_add(conv, bias)
    relu = tf.nn.relu(pre_activation) if add_relu else pre_activation
    after_norm = norm('norm' + str(name), relu, lsize=4)
    return after_norm


def residual_block(input, name, in_channel, neck_channel, out_channel, trunk):
    _strides = [1, 2, 2, 1] if name.startswith("res3a") or name.startswith("res4a") else [1, 1, 1, 1]
    res = conv_block(input, name + '_branch2a', shape=[1, 1, in_channel, neck_channel],
                     strides=_strides, padding="VALID", add_relu=True)
    res = conv_block(res, name + '_branch2b', shape=[3, 3, neck_channel, neck_channel],
                     padding="SAME", add_relu=True)
    res = conv_block(res, name + '_branch2c', shape=[1, 1, neck_channel, out_channel],
                     padding="VALID", add_relu=False)
    res = trunk + res
    res = tf.nn.relu(res)
    return res


def resnet(_X):
    print("_X", _X.get_shape())
    conv1 = conv_block(_X, 'conv1', shape=[7, 7, 3, 64], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv1", conv1.get_shape())
    pool1 = tf.nn.max_pool(conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')
    print("pool1", pool1.get_shape())

    res2a_branch1 = conv_block(pool1, 'res2a_branch1', shape=[1, 1, 64, 256], padding="VALID", add_relu=False)
    print("res2a_branch1", res2a_branch1.get_shape())
    res2a = residual_block(pool1, 'res2a', 64, 64, 256, res2a_branch1)
    print("res2a", res2a.get_shape())
    res2b = residual_block(res2a, 'res2b', 256, 64, 256, res2a)
    print("res2b", res2b.get_shape())
    res2c = residual_block(res2b, 'res2c', 256, 64, 256, res2b)
    print("res2c", res2c.get_shape())

    res3a_branch1 = conv_block(res2c, 'res3a_branch1', shape=[1, 1, 256, 512], strides=[1, 2, 2, 1], padding="VALID", add_relu=False)
    print("res3a_branch1", res3a_branch1.get_shape())
    res3a = residual_block(res2c, 'res3a', 256, 128, 512, res3a_branch1)
    print("res3a", res3a.get_shape())

    res3b1 = residual_block(res3a, 'res3b1', 512, 128, 512, res3a)
    print("res3b1", res3b1.get_shape())
    res3b2 = residual_block(res3b1, 'res3b2', 512, 128, 512, res3b1)
    print("res3b2", res3b2.get_shape())
    res3b3 = residual_block(res3b2, 'res3b3', 512, 128, 512, res3b2)
    print("res3b3", res3b3.get_shape())

    conv2 = conv_block(res3b3, 'conv2', shape=[7, 7, 512, 64], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv2", conv2.get_shape())

    conv3 = conv_block(conv2, 'conv3', shape=[7, 7, 64, 16], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv3", conv3.get_shape())

    dense = tf.reshape(conv3, [-1, weights['wd1'].get_shape().as_list()[0]])
    print("dense", dense.get_shape())
    out = tf.matmul(dense, weights['wd1']) + biases['bd1']
    print("out", out.get_shape())
    return out

with tf.variable_scope('variables', reuse=tf.AUTO_REUSE):
    weights = {
        'wd1': tf.get_variable('wd1', [320, 4], initializer=tf.orthogonal_initializer())
    }
    biases = {
        'bd1': tf.get_variable('bd1', [4])
    }

with tf.variable_scope('final', reuse=tf.AUTO_REUSE):
    pred = resnet(x)
    saver = tf.train.Saver()

with tf.Session() as sess:
  # Initialize variables
  load_path = saver.restore(sess, model_path)
  np_y_pred = sess.run(pred, feed_dict={x: inputs, keep_prob: 1.0})
  img = Image.open(image_path)
  img_weight, img_height = img.size
  np_y_pred = np_y_pred / np.array([120, 160, 120, 160]) * np.array([img_weight, img_height, img_weight, img_height])
  print(np_y_pred)
  x1 = int((np_y_pred[0][0] - np_y_pred[0][2] / 2))
  x2 = int((np_y_pred[0][1] - np_y_pred[0][3] / 2))
  x3 = int((np_y_pred[0][0] + np_y_pred[0][2] / 2))
  x4 = int((np_y_pred[0][1] + np_y_pred[0][3] / 2))

  x1 = min(max(x1, 0), img_weight)
  x2 = min(max(x2, 0), img_height)
  x3 = min(max(x3, 0), img_weight)
  x4 = min(max(x4, 0), img_height)

  draw = ImageDraw.Draw(img)
  draw.polygon([(x1,x2),(x3,x2),(x3,x4),(x1,x4)], outline=(255,0,0))
  plt.imshow(img)
  plt.show()
  #img.paste((256, 256, 0), (x1, x2, x3, x4))