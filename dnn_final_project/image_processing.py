from PIL import Image
import math
import numpy as np
import os

def image_preprocessing():
    datas = []
    index = 0
    data = []
    for i in range(1, 11):
        file_path = 'data/FDDB-fold-0' + str(i) + '-ellipseList.txt'
        if i == 10:
            file_path = 'data/FDDB-fold-' + str(i) + '-ellipseList.txt'
        f = open(file_path, encoding="utf-8")
        for line in f:
            data.append(line.split("\n")[0])
            if index == 1:
                num_faces = int(line)
            elif index > 1:
                num_faces = num_faces - 1
                if num_faces == 0:
                    datas.append(data.copy())
                    data = []
                    index = -1
            index = index + 1
    dataes = []
    for i in range(len(datas)):
        if datas[i][1] == '1':
            dataes.append(datas[i])

    for i in range(len(dataes)):
        print(i)
        print(dataes[i][0])
        img = Image.open('image/' + dataes[i][0] + '.jpg')
        #img.resize((128, 128)).save('/Users/chenyujie/Desktop/123/' + str(i) + '.jpg')

        img.resize((120, 160)).save('processed_image/' + dataes[i][0] + '.jpg')


        #os.remove("/Users/chenyujie/Desktop/123" + str(i) + ".jpg")
image_preprocessing()