from PIL import Image
import math
import numpy as np

def preprocessing():
    datas = []
    index = 0
    data = []
    for i in range(1, 10):
        file_path = 'data/FDDB-fold-0' + str(i) + '-ellipseList.txt'
        f = open(file_path, encoding="utf-8")
        for line in f:
            data.append(line.split("\n")[0])
            if index == 1:
                num_faces = int(line)
            elif index > 1:
                num_faces = num_faces - 1
                if num_faces == 0:
                    datas.append(data.copy())
                    data = []
                    index = -1
            index = index + 1
    dataes = []
    for i in range(len(datas)):
        if datas[i][1] == '1':
            dataes.append(datas[i])

    outputs = fddb_convert_to_darknet(dataes)
    inputs = []
    labels = []
    for i in range(len(outputs)):
    #for i in range(3):
        print(i)
        img = Image.open('processed_image/' + outputs[i][0] + '.jpg')
        img_weight, img_height = img.size
        image = np.array(img.getdata())
        input = np.reshape(image, (img_weight, img_height, 3))
        inputs.append(input)
        label = [outputs[i][1], outputs[i][2], outputs[i][3], outputs[i][4]]
        labels.append(label)
        #x1 = int((outputs[i][1] - outputs[i][3] / 2) * img_weight)
        #x2 = int((outputs[i][2] - outputs[i][4] / 2) * img_height)
        #x3 = int((outputs[i][1] + outputs[i][3] / 2) * img_weight)
        #x4 = int((outputs[i][2] + outputs[i][4] / 2) * img_height)

        #img.paste((256, 256, 0), (x1, x2, x3, x4))  ##(256,256,0)表示黄色
        #img.show()
    inputs = np.array(inputs)
    labels = np.array(labels)
    labels = labels * np.array([120, 160, 120, 160])
    print("data loaded")
    return inputs, labels, outputs


def fddb_convert_to_darknet(dataes):
    outputs = []
    for i in range(len(dataes)):
        img = Image.open('image/' + dataes[i][0] + '.jpg')
        if img.mode == 'RGB':
            img_width, img_height = img.size

            current_line_split = dataes[i][2].split()
            major_axis_radius = float(current_line_split[0])
            minor_axis_radius = float(current_line_split[1])
            angle = float(current_line_split[2])
            center_x = float(current_line_split[3])
            center_y = float(current_line_split[4])

            calc_x = math.sqrt(major_axis_radius ** 2 * math.cos(angle) ** 2 + minor_axis_radius ** 2 * math.sin(angle) ** 2)
            calc_y = math.sqrt(major_axis_radius ** 2 * math.sin(angle) ** 2 + minor_axis_radius ** 2 * math.cos(angle) ** 2)

            bbox_x = center_x / img_width
            bbox_y = center_y / img_height
            bbox_w = (2 * calc_x) / img_width
            bbox_h = (2 * calc_y) / img_height
            output = [dataes[i][0], bbox_x, bbox_y, bbox_w, bbox_h]
            outputs.append(output)
    print(len(outputs))
    return outputs
