import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image,ImageDraw
#import cv2
#import dlib
#import sys

import data
inputs, labels, outputs = data.preprocessing()


batch_size = 300
learning_rate = 0.001
decay_learning_rate = 0.995
training_iters = 2000

n_input = [120, 160, 3]
n_output = 4
dropout = 0.6

x = tf.placeholder(tf.float32, [None, n_input[0], n_input[1], n_input[2]])
y = tf.placeholder(tf.float32, [None, n_output])
keep_prob = tf.placeholder(tf.float32)
lr = tf.placeholder(tf.float32)
model_path = "model.ckpt"

weights = {}
bias = {}


def norm(name, l_input, lsize=4):
    return tf.nn.lrn(l_input, lsize, bias=1.0, alpha=0.001 / 9.0, beta=0.75, name=name)


def _weight_variable(name, shape):
    return tf.get_variable("w" + str(name), shape, initializer=tf.orthogonal_initializer())


def _bias_variable(name, shape):
    return tf.get_variable("b" + str(name), shape[3])


def conv_block(input, name, shape, strides=[1, 1, 1, 1], padding="SAME", add_relu=True):
    weight = _weight_variable(name, shape)
    bias = _bias_variable(name, shape)
    conv = tf.nn.conv2d(input, weight, strides, padding=padding)
    pre_activation = tf.nn.bias_add(conv, bias)
    relu = tf.nn.relu(pre_activation) if add_relu else pre_activation
    after_norm = norm('norm' + str(name), relu, lsize=4)
    return after_norm


def residual_block(input, name, in_channel, neck_channel, out_channel, trunk):
    _strides = [1, 2, 2, 1] if name.startswith("res3a") or name.startswith("res4a") else [1, 1, 1, 1]
    res = conv_block(input, name + '_branch2a', shape=[1, 1, in_channel, neck_channel],
                     strides=_strides, padding="VALID", add_relu=True)
    res = conv_block(res, name + '_branch2b', shape=[3, 3, neck_channel, neck_channel],
                     padding="SAME", add_relu=True)
    res = conv_block(res, name + '_branch2c', shape=[1, 1, neck_channel, out_channel],
                     padding="VALID", add_relu=False)
    res = trunk + res
    res = tf.nn.relu(res)
    return res


def resnet(_X):
    print("_X", _X.get_shape())
    conv1 = conv_block(_X, 'conv1', shape=[7, 7, 3, 64], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv1", conv1.get_shape())
    pool1 = tf.nn.max_pool(conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')
    print("pool1", pool1.get_shape())

    res2a_branch1 = conv_block(pool1, 'res2a_branch1', shape=[1, 1, 64, 256], padding="VALID", add_relu=False)
    print("res2a_branch1", res2a_branch1.get_shape())
    res2a = residual_block(pool1, 'res2a', 64, 64, 256, res2a_branch1)
    print("res2a", res2a.get_shape())
    res2b = residual_block(res2a, 'res2b', 256, 64, 256, res2a)
    print("res2b", res2b.get_shape())
    res2c = residual_block(res2b, 'res2c', 256, 64, 256, res2b)
    print("res2c", res2c.get_shape())

    res3a_branch1 = conv_block(res2c, 'res3a_branch1', shape=[1, 1, 256, 512], strides=[1, 2, 2, 1], padding="VALID", add_relu=False)
    print("res3a_branch1", res3a_branch1.get_shape())
    res3a = residual_block(res2c, 'res3a', 256, 128, 512, res3a_branch1)
    print("res3a", res3a.get_shape())

    res3b1 = residual_block(res3a, 'res3b1', 512, 128, 512, res3a)
    print("res3b1", res3b1.get_shape())
    res3b2 = residual_block(res3b1, 'res3b2', 512, 128, 512, res3b1)
    print("res3b2", res3b2.get_shape())
    res3b3 = residual_block(res3b2, 'res3b3', 512, 128, 512, res3b2)
    print("res3b3", res3b3.get_shape())

    conv2 = conv_block(res3b3, 'conv2', shape=[7, 7, 512, 64], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv2", conv2.get_shape())

    conv3 = conv_block(conv2, 'conv3', shape=[7, 7, 64, 16], strides=[1, 2, 2, 1], padding="SAME", add_relu=True)
    print("conv3", conv3.get_shape())

    dense = tf.reshape(conv3, [-1, weights['wd1'].get_shape().as_list()[0]])
    print("dense", dense.get_shape())
    out = tf.matmul(dense, weights['wd1']) + biases['bd1']
    print("out", out.get_shape())
    return out

with tf.variable_scope('variables', reuse=tf.AUTO_REUSE):
    weights = {
        'wd1': tf.get_variable('wd1', [320, 4], initializer=tf.orthogonal_initializer())
    }
    biases = {
        'bd1': tf.get_variable('bd1', [4])
    }

def get_batch(inputs, labels, batch_size):
    num_ele = inputs.shape[0]
    num_batch = int(num_ele / batch_size)
    inputs_batch = []
    labels_batch = []
    for i in range(num_batch):
        inputs_batch.append(inputs[i * batch_size: (i + 1) * batch_size])
        labels_batch.append(labels[i * batch_size: (i + 1) * batch_size])
    return inputs_batch, labels_batch, num_batch

with tf.variable_scope('final', reuse=tf.AUTO_REUSE):
    pred = resnet(x)
    pred_split1 = tf.split(pred, 4, axis=1)
    y_split = tf.split(y, 4, axis=1)
    cost = tf.reduce_sum(tf.pow(tf.subtract(pred, y), 2)) / batch_size
    optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)
    init = tf.initialize_all_variables()
    inputs_batch, labels_batch, num_batch = get_batch(inputs, labels, batch_size)
    saver = tf.train.Saver()
'''
with tf.Session() as sess:
    sess.run(init)
    step = 0
    count = 0
    while step < training_iters:
        batch_xs, batch_ys = inputs_batch[count], labels_batch[count]

        learning_rate = learning_rate * decay_learning_rate
        sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys, keep_prob: dropout, lr: learning_rate})
        loss = sess.run(cost, feed_dict={x: batch_xs, y: batch_ys, keep_prob: 1.})
        print(step, "   ", loss)
        step += 1
        if step % 100 == 0:
            count = (count + 1) % num_batch
    save_path = saver.save(sess, model_path)
    print("Model saved in file: %s" % save_path)
    print("Optimization Finished!")'''

learning_rate = 0.00003
decay_learning_rate = 0.995
training_iters = 500
with tf.Session() as sess:
    # Initialize variables
    load_path = saver.restore(sess, model_path)
    print("Model restored from file: %s" % load_path)
    step = 0
    count = 0
    while step < training_iters:
        batch_xs, batch_ys = inputs_batch[count], labels_batch[count]
        learning_rate = learning_rate * decay_learning_rate
        sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys, keep_prob: dropout, lr: learning_rate})
        loss = sess.run(cost, feed_dict={x: batch_xs, y: batch_ys, keep_prob: dropout})
        print(step, "   ", loss)
        step += 1
        count = (count + 1) % num_batch
    save_path = saver.save(sess, model_path)
    print("Model saved in file: %s" % save_path)
    print("Optimization Finished!")

with tf.Session() as sess:
  # Initialize variables
  load_path = saver.restore(sess, model_path)
  output = sess.run(pred, feed_dict={x: inputs[0:300], keep_prob: dropout})
  for i in range(1,5):
    o = sess.run(pred, feed_dict={x: inputs[batch_size*i : batch_size*(i+1)], keep_prob: dropout})
    output = np.vstack((output, o))
y_label = labels[0:1500]
output_split = np.split(output, 4, axis=1)
output_split[2] = np.maximum(output_split[2], 0)
output_split[3] = np.maximum(output_split[3], 0)
y_split = np.split(y_label, 4, axis=1)
wid = np.maximum((y_split[2] + output_split[2]) / 2 - np.abs(y_split[0] - output_split[0]), 0)
hig = np.maximum((y_split[3] + output_split[3]) / 2 - np.abs(y_split[1] - output_split[1]), 0)
square_intersection = wid * hig
square_joint = y_split[2] * y_split[3] + output_split[2] * output_split[3] - square_intersection
accuracy = np.sum(square_intersection / square_joint)/1500
print(accuracy)


with tf.Session() as sess:
  # Initialize variables
  load_path = saver.restore(sess, model_path)
  np_y_pred = sess.run(pred, feed_dict={x: inputs[0:10], keep_prob: 1.0})
  #print(labels)
  #print(np_y_pred)
  for i in range(10):
    img = Image.open('processed_image/' + outputs[i][0] + '.jpg')
    img_weight, img_height = img.size
    print()
    x1 = int((np_y_pred[i][0] - np_y_pred[i][2] / 2))
    x2 = int((np_y_pred[i][1] - np_y_pred[i][3] / 2))
    x3 = int((np_y_pred[i][0] + np_y_pred[i][2] / 2))
    x4 = int((np_y_pred[i][1] + np_y_pred[i][3] / 2))

    x1 = min(max(x1, 0), n_input[0])
    x2 = min(max(x2, 0), n_input[1])
    x3 = min(max(x3, 0), n_input[0])
    x4 = min(max(x4, 0), n_input[1])
    draw = ImageDraw.Draw(img)
    draw.polygon([(x1,x2),(x3,x2),(x3,x4),(x1,x4)], outline=(0,255,0))
    plt.imshow(img)
    plt.show()
    #img.paste((256, 256, 0), (x1, x2, x3, x4))
