import numpy as np
import warnings



def batchnorm_forward(x, gamma, beta, bn_param):
    """
    Forward pass for batch normalization.

    During training the sample mean and (uncorrected) sample variance are
    computed from minibatch statistics and used to normalize the incoming data.

    During training we also keep an exponentially decaying running mean of the
    mean and variance of each feature, and these averages are used to normalize
    data at test-time.

    At each timestep we update the running averages for mean and variance using
    an exponential decay based on the momentum parameter:

    running_mean = momentum * running_mean + (1 - momentum) * sample_mean
    running_var = momentum * running_var + (1 - momentum) * sample_var

    Note that the batch normalization paper suggests a different test-time
    behavior: they compute sample mean and variance for each feature using a
    large number of training images rather than using a running average. For
    this implementation we have chosen to use running averages instead since
    they do not require an additional estimation step; the torch7
    implementation of batch normalization also uses running averages.

    Input:
    - x: Data of shape (N, D)
    - gamma: Scale parameter of shape (D,)
    - beta: Shift paremeter of shape (D,)
    - bn_param: Dictionary with the following keys:
      - mode: 'train' or 'test'; required
      - eps: Constant for numeric stability
      - momentum: Constant for running mean / variance.
      - running_mean: Array of shape (D,) giving running mean of features
      - running_var Array of shape (D,) giving running variance of features

    Returns a tuple of:
    - out: of shape (N, D)
    """
    mode = bn_param['mode']
    eps = bn_param['eps']
    momentum = bn_param['momentum']
    running_mean = bn_param['running_mean']
    running_var = bn_param['running_var'] 

    out = None
    if mode == 'train':
        
        sample_mean = np.mean(x, axis = 0)
        sample_var = np.var(x, axis = 0)
        
        x_normalize = (x - sample_mean)/np.sqrt(sample_var + eps)
        x_scale_shift = gamma * x_normalize + beta
        
        running_mean = momentum * running_mean + (1 - momentum) * sample_mean
        running_var = momentum * running_var + (1 - momentum) * sample_var
        
        out = x_scale_shift
        
    elif mode == 'test':
        
        x_normalize = (x - running_mean)/np.sqrt(running_var + eps)
        x_scale_shift = gamma * x_normalize + beta
        out = x_scale_shift
        
    else:
        raise ValueError('Invalid forward batchnorm mode "%s"' % mode)

    # Store the updated running means back into bn_param
    bn_param['running_mean'] = running_mean
    bn_param['running_var'] = running_var

    return out




def dropout_forward(x, dropout_param):
    """
    Performs the forward pass for (inverted) dropout.

    Inputs:
    - x: Input data, of any shape
    - dropout_param: A dictionary with the following keys:
      - p: Dropout parameter. We keep each neuron output with probability p.
      - mode: 'test' or 'train'. If the mode is train, then perform dropout;
        if the mode is test, then just return the input.
      - seed: Seed for the random number generator. Passing seed makes this
        function deterministic, which is needed for gradient checking but not
        in real networks.

    Outputs:
    - out: Array of the same shape as x.

    NOTE: Please implement **inverted** dropout, not the vanilla version of dropout.
    Namely, divide the the training output by p, and do nothing for testing

    NOTE 2: Keep in mind that p is the probability of **keep** a neuron
    output; this might be contrary to some sources, where it is referred to
    as the probability of dropping a neuron output.
    """
    p, mode = dropout_param['p'], dropout_param['mode']
    if 'seed' in dropout_param:
        np.random.seed(dropout_param['seed'])

    mask = ((np.random.random(x.shape) < p)-0)/p
    out = None

    if mode == 'train':
        out = x * mask
        
    elif mode == 'test':
        out = x

    return out



def conv_forward_naive(x, w, b, conv_param):
    """
    A naive implementation of the forward pass for a convolutional layer.

    The input consists of N data points, each with C channels, height H and
    width W. We convolve each input with F different filters, where each filter
    spans all C channels and has height HH and width WW.

    Input:
    - x: Input data of shape (N, H, W, C)
    - w: Filter weights of shape (HH, WW, C, F)
    - b: Biases, of shape (F,)
    - conv_param: A dictionary with the following keys:
      - 'stride': The number of pixels between adjacent receptive fields in the
        horizontal and vertical directions.
      - 'pad': The number of pixels that will be used to zero-pad the input. 
        

    During padding, 'pad' zeros should be placed symmetrically (i.e equally on both sides)
    along the height and width axes of the input. Be careful not to modfiy the original
    input x directly.

    Returns a tuple of:
    - out: Output data, of shape (N, H', W', F) where H' and W' are given by
      H' = 1 + (H + 2 * pad - HH) / stride
      W' = 1 + (W + 2 * pad - WW) / stride
    """
  
    N, H, W, C = x.shape
    hh, ww, C, F = w.shape
    S = conv_param['stride']
    P = conv_param['pad']
    x_pad = np.pad(x, ((0,), (P,), (P,), (0,)), 'constant')
    
    Hh = int(1 + (H + 2 * P - hh) / S)
    Ww = int(1 + (W + 2 * P - ww) / S)
    out = np.zeros((N, Hh, Ww, F))
    
    w = np.transpose(w, [3, 0, 1, 2])
    for n in range(N):
        for hi in range(Hh):
            for wi in range(Ww):
                for f in range(F):
                    out[n,hi,wi,f] = np.sum(x_pad[n, hi*S : hi*S + hh, wi*S : wi*S + ww, :] * w[f,:,:,:]) + b[f]

    return out



def max_pool_forward_naive(x, pool_param):
    """
    A naive implementation of the forward pass for a max-pooling layer.

    Inputs:
    - x: Input data, of shape (N, H, W, C)
    - pool_param: dictionary with the following keys:
      - 'pool_height': The height of each pooling region
      - 'pool_width': The width of each pooling region
      - 'stride': The distance between adjacent pooling regions

    No padding is necessary here. Output size is given by 

    Returns a tuple of:
    - out: Output data, of shape (N, C, H', W') where H' and W' are given by
      H' = 1 + (H - pool_height) / stride
      W' = 1 + (W - pool_width) / stride
    """
    out = None
    N,H,W,C = x.shape
    ph = pool_param['pool_height']
    pw = pool_param['pool_width']
    s = pool_param['stride']
    Hh = int(1 + (H - ph) / s)
    Ww = int(1 + (W - pw) / s)
    out = np.zeros((N, Hh, Ww, C))
    for n in range(N):
        for c in range(C):
            for h in range(Hh):
                for w in range(Ww):
                    out[n, h, w, c] = np.max(x[n, h*s : h*s + ph, w*s : w*s + pw, c])
    
    return out


